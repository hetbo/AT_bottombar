package ir.hetbo.at_bottombar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomBar bottomBar = (BottomBar)findViewById(R.id.bottomBar);

        final Fragment home = new Home();
        final Fragment other = new Other();
        final Fragment profile = new Profile();
        final Fragment setting = new Setting();

        final FragmentManager fragmentManager = getSupportFragmentManager();

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId) {
                    case R.id.tab_home:
                        fragmentManager.beginTransaction().replace(R.id.Container,home).commit();
                        break;
                    case R.id.tab_profile:
                        fragmentManager.beginTransaction().replace(R.id.Container,profile).commit();
                        break;
                    case R.id.tab_other:
                        fragmentManager.beginTransaction().replace(R.id.Container,other).commit();
                        break;
                    case R.id.tab_setting:
                        fragmentManager.beginTransaction().replace(R.id.Container,setting).commit();
                        break;
                }

            }});


        Button web = (Button)findViewById(R.id.web);
        Button tel = (Button)findViewById(R.id.tel);
        web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("http://hetbo.ir"));
                startActivity(i);            }
        });

        tel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://telegram.me/hetboteam"));
                startActivity(i);
            }
        });

    }

}
